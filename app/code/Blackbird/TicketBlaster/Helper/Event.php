<?php

namespace Blackbird\TicketBlaster\Helper;

class Event extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * Create just a useful method for our extension
     *
     * @return bool
     */
    public function justAUsefulMethod(){
        // Real code here
        // ...
        return true;
    }
}